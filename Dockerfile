ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

FROM hadolint/hadolint:v1.18.2 AS hadolint-dist
FROM koalaman/shellcheck:v0.7.1 AS shellcheck-dist
FROM greut/eclint:v0.2.9 AS eclint-dist
FROM wata727/tflint:0.20.3 AS tflint-dist
FROM alpine/helm:3.4.0 AS helm-dist
FROM node:15-alpine AS base

FROM base AS builder

RUN mkdir -p /install/bin
WORKDIR /install
ENV PATH=/install/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

COPY --from=hadolint-dist /bin/hadolint /install/bin/hadolint
RUN hadolint --version

COPY --from=shellcheck-dist /bin/shellcheck /install/bin/shellcheck
RUN shellcheck --version

COPY --from=tflint-dist /usr/local/bin/tflint /install/bin/tflint
RUN tflint --version

COPY --from=helm-dist /usr/bin/helm /install/bin/helm
RUN helm version

COPY --chown=root:root ci-lint.sh /install/bin/ci-lint
RUN set -eux \
  ; chmod 0755 /install/bin/ci-lint \
  ; mkdir -p /install/src \
  ; chown 1042:1042 /install/src

FROM base AS rootfs

COPY --from=builder /install /

RUN set -eux \
  ; adduser -h /home/ci -D -u 1042 -s /bin/bash -g CI ci \
  ; apk add --no-cache --update bash=5.0.11-r1 \
  ; bash --version \
  ; yarn global add eslint@7.12.1 \
  ; eslint --version

FROM scratch

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="ci-lint" \
  org.label-schema.description="Automated syntax validation with multiply tools included" \
  org.label-schema.url="https://gitlab.com/4ops/ci-lint" \
  org.label-schema.vcs-ref="${VCS_REF}" \
  org.label-schema.version="${VERSION}" \
  org.label-schema.vcs-url="https://gitlab.com/4ops/ci-lint" \
  org.label-schema.vendor="Anton Kulikov" \
  org.label-schema.schema-version="1.0"

COPY --from=rootfs / /

USER 1042:1042
WORKDIR /src
VOLUME [ "/src" ]
CMD [ "ci-lint" ]
