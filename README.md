# CI Lint

## Usage

CLI:

```console
docker run --rm -it --volume "$(pwd):/src:ro" 4ops/ci-lint
```

GitLab:

```yaml
lint:
  image: 4ops/ci-lint
  script: ci-lint
```
