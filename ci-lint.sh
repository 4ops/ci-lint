#!/usr/bin/env bash

set -o pipefail
set -o errtrace
set -o nounset
set -o errexit

[ -r Chart.yaml ] && helm version && helm lint .
[ -r Dockerfile ] && hadolint --version && hadolint Dockerfile
[ -r main.tf ] && tflint --version && tflint .
[ -r package.json ] && eslint --version && eslint .

find . -name '*.sh' -exec shellcheck {} +
